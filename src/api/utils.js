import urls from './urls';

export const fetchUtil = async (url) => {
  try {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error('Something wrong with response');
    }
    const data = await response.json();

    return data;
  } catch (e) {
    console.log((e && e.message) || e);
  }
};

export const getStories = async () => {
  try {
    const storyIds = await fetchUtil(urls.topStoryIds);
    const randomIds = new Set();

    while (randomIds.size < 10) {
      randomIds.add(storyIds[Math.round(Math.random() * storyIds.length - 1)]);
    }

    return await Promise.all([...randomIds]
      .map((id) => fetchUtil(urls.item(id))));
  } catch (e) {
    console.log(e);
  }
};

export const getAuthors = async (stories) => {
  try {
    const authorIds = stories.map(({ by }) => by);

    return await Promise.all(authorIds
      .map((id) => fetchUtil(urls.user(id))));
  } catch (e) {
    console.log(e);
  }
};

export const getComments = async (commentIds) =>{
  try {
    return await Promise.all(commentIds.map(
      (commentId) => fetchUtil(urls.item(commentId)),
    ));
  } catch (e) {
    console.log(e)
  }
};
