export default {
  topStoryIds: 'https://hacker-news.firebaseio.com/v0/topstories.json',
  item: (id) => `https://hacker-news.firebaseio.com/v0/item/${id}.json`,
  user: (id) => `https://hacker-news.firebaseio.com/v0/user/${id}.json`,
};
