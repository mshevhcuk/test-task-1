import React from 'react';
import './styles.css';

const Spinner = ({ size = 16, text }) => (
  <span
    style={{
      minWidth: size * 2, fontSize: size,
    }}
    className="spinner"
  >
    {text}
  </span>
);

export default Spinner;
