import React, { useState, useEffect } from 'react';

import { fetchUtil } from '../api/utils';
import urls from '../api/urls';
import Spinner from './spinner';

import './styles.css';

const Comment = ({ comment = {} }) => {
  const [commentsCount, setCommentsCount] = useState(null);
  const [isLoding, setIsLoading] = useState(false);

  useEffect(() => {
    const fetchCommentsCount = async () => {
      setIsLoading(true);

      const author = await fetchUtil(urls.user(comment.by));
      setCommentsCount(author.submitted.length);
      setIsLoading(false);
    };

    fetchCommentsCount();
  }, [comment.by]);

  return (
    <div className="comment">
      <small>
        Date:
        <span>{new Date(comment.time * 1000).toLocaleString()}</span>
      </small>
      <small>
        Author:
        <span>{comment.by}</span>
      </small>
      <small>
        Overall comments:
        {isLoding ? <Spinner size={15} text="loading" /> : <span>{commentsCount}</span>}
      </small>
    </div>
  );
};

export default Comment;
