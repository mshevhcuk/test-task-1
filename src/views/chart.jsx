import React from 'react';
import { useSelector } from 'react-redux';
import { Line } from 'react-chartjs-2';

import { getStories } from '../redux/selectors';

const Chart = () => {
  const stories = useSelector(getStories);

  const data = {
    labels: stories.map((_, index) => index),
    datasets: [
      {
        label: 'Scores',
        fill: false,
        lineTension: 0.1,
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderCapStyle: 'butt',

        pointBorderWidth: 1,
        pointHoverRadius: 5,
        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
        pointHoverBorderColor: 'rgba(220,220,220,1)',
        pointHoverBorderWidth: 2,

        data: stories.map(({ score }) => score),
      },
    ],
  };

  return (
    <Line
      data={data}
      height={500}
      options={{ maintainAspectRatio: false }}
    />
  );
};

export default Chart;
