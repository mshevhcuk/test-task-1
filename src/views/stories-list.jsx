import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

import { getSortedStories, getIsStoriesLoading } from '../redux/selectors';
import Story from './story';
import Spinner from './spinner';

const StoriesList = () => {
  const stories = useSelector(getSortedStories);
  const isStoriesLoading = useSelector(getIsStoriesLoading);
  const history = useHistory();

  return (
    isStoriesLoading ? <Spinner text="Random stories are loading...." size={32} />
      : (
        <div>
          {stories.map((story) => <Story key={story.id} story={story} onClick={() => history.push(`/story/${story.id}`)} />)}
        </div>
      )
  );
};
export default StoriesList;
