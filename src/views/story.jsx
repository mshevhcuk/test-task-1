import React from 'react';

import Spinner from './spinner';
import './styles.css';

const Story = ({ story = {}, onClick = () => {} }) => (
  <div onClick={onClick} className="story-container">
    <div className="story-header">{story.title}</div>
    <div className="details-container">
      <small>
        Score:
        <span>{story.score}</span>
      </small>
      <small>
        Author:
        <span>{story.by}</span>
      </small>
      <small>
        Authors karma:
        {!story.authorKarma ? <Spinner text="is loading" size={16} /> : <span>{story.authorKarma}</span>}
      </small>
      <small>
        Date created:
        <span>{new Date(story.time * 1000).toLocaleString()}</span>
      </small>
    </div>
  </div>
);

export default Story;
