import React, { useEffect, useState } from 'react';
import { useParams, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { getStoryById } from '../redux/selectors';
import { getComments } from '../api/utils';

import Comment from './comment';
import Story from './story';
import Spinner from './spinner';

import './styles.css';

const StoryDetails = () => {
  const [comments, setComments] = useState([]);
  const [isLoding, setIsLoading] = useState(false);

  const { id } = useParams();

  const story = useSelector(getStoryById(id));

  const commentsCount = story && story.kids ? story.kids.length : 0;

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);

      if (!story || !commentsCount) {
        return;
      }
      
      const commentsIds = commentsCount > 10 ? story.kids.slice(0, 10) : story.kids;

      const storyComments = await getComments(commentsIds);
      setComments(storyComments);

      setIsLoading(false);
    };

    fetchData();
  }, [commentsCount, story]);

  if (!story) {
    return <Redirect to="/" />;
  }

  return (
    <div>
      <Story story={story} />
      <div>View story source:</div>
      <button type="button">
        <a href={story.url} rel="noopener noreferrer" target="_blank">{story.url}</a>
      </button>
      <div className="comments-container">
        {commentsCount
          ? (
            <div>
              {commentsCount > 10
                ? 'First 10 comments'
                : `${commentsCount} comments`}
            </div>
          ) : <div>Nobody has left a comment yet</div> }
        {isLoding ? <Spinner size={32} text="Comments are loading..." /> : comments.map((comment) => <Comment key={comment.id} comment={comment} />)}
      </div>
    </div>
  );
};

export default StoryDetails;
