import {
  FETCH_STORIES_LOADING,
  FETCH_AUTHORS_SUCCESS,
  FETCH_STORIES_SUCCESS,
} from './actionTypes';

const initialState = {
  stories: [],
  isAuthorsLoading: false,
  isStoriesLoading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_STORIES_SUCCESS: {
      const { stories } = action.payload;

      return { ...state, stories, isStoriesLoading: false };
    }

    case FETCH_AUTHORS_SUCCESS: {
      const authorKarmasMap = action.payload.authors.reduce(
        (acc, { id, karma }) => ({ ...acc, [id]: karma }),
        {},
      );
      const enhancedStories = state.stories.map(
        (story) => ({ ...story, authorKarma: authorKarmasMap[story.by] }),
      );

      return { ...state, stories: enhancedStories, isAuthorsLoading: false };
    }

    case FETCH_STORIES_LOADING: return { ...state, isStoriesLoading: true };

    default:
      return state;
  }
};
