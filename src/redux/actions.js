import { getAuthors, getStories } from '../api/utils';
import {
  FETCH_STORIES_LOADING,
  FETCH_AUTHORS_SUCCESS,
  FETCH_STORIES_SUCCESS,
} from './actionTypes';

export const fetchStoriesSuccessAction = (stories) => ({
  type: FETCH_STORIES_SUCCESS,
  payload: { stories },
});

export const fetchAuthorsSuccessAction = (authors) => ({
  type: FETCH_AUTHORS_SUCCESS,
  payload: { authors },
});

export const fetchStoriesLoadingAction = () => ({
  type: FETCH_STORIES_LOADING,
});

export const initialLoad = () => async (dispatch) => {
  dispatch(fetchStoriesLoadingAction());

  const stories = await getStories();
  dispatch(fetchStoriesSuccessAction(stories));

  const authors = await getAuthors(stories);
  dispatch(fetchAuthorsSuccessAction(authors));
};
