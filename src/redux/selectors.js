import { createSelector } from 'reselect';

export const getStories = createSelector(
  [(state) => state.stories],
  (stories) => stories,
);

export const getSortedStories = createSelector(
  [(state) => state.stories],
  (stories) => [...stories].sort((a,b) => b.score - a.score)
);

export const getStoryById = (id) => createSelector(
  [getStories],
  (stories) => stories.find((item) => item.id.toString() === id)
);

export const getIsStoriesLoading = createSelector(
  [(store) => store.isStoriesLoading],
  (isLoading) => isLoading
);
