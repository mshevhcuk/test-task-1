import React from 'react';
import { Route, Switch, Redirect, Link } from 'react-router-dom';

import { StoriesList, StoryDetails, Chart } from './views';

const goToChartLink = <Link className="chart-link link" to="/chart">Go to chart</Link>;
const goBackLink = <Link className="home-link link" to="/">Go back</Link>;

const Routes = () => (
  <Switch>
    <Route exact path="/">
      {goToChartLink}
      <StoriesList />
    </Route>
    <Route path="/story/:id">
      {goToChartLink}
      {goBackLink}
      <StoryDetails />
    </Route>
    <Route path="/chart">
      {goBackLink}
      <Chart />
    </Route>
    <Route>
      <Redirect to="/" />
    </Route>
  </Switch>
);

export default Routes;
